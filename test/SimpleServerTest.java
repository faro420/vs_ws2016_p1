package test;

import java.rmi.RemoteException;

import server.Server;

public class SimpleServerTest {
	
	private static final String ID = "Client1";
	private static final String ID2 = "Client2";
	
	public static void main ( String [] args ) {
		try {
			Server server = new Server();
			
			System.out.println("Starting Server test!");
			System.out.println("Sending 1 Message!");
			
			server.newMessage(ID, "First Message!");
			Thread.sleep(1000);
			
			System.out.println("Receiving (1 new) after 1 second!");
			
			System.out.println(server.nextMessage(ID));
			Thread.sleep(1000);
			
			System.out.println("Receiving (0 new) after 2 second!");
			
			System.out.println(server.nextMessage(ID));
			
			System.out.println("Sending 1 Message!");
			
			server.newMessage(ID, "Second Message!");
			Thread.sleep(1000);
			
			System.out.println("Receiving (1 new) after 1 second!");
			
			System.out.println(server.nextMessage(ID));
			
			System.out.println("Receiving (2 new) after 1 second!");
			
			System.out.println(server.nextMessage(ID2));
			
			System.out.println("Waiting till handle is invalid! (~10sec)");
			
			Thread.sleep(10500);
			
			System.out.println("Expecting (2 old new)!");
			
			System.out.println(server.nextMessage(ID));
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
