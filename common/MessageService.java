/**
 * 
 */
package common;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Project: VS_1
 *
 * @author Andreas M�ller
 * @MatrNr 209 918 2
 *
 * 24.09.2016
 */
public interface MessageService extends Remote {
	
	/**
	 * Next message.
	 *
	 * @param clientID the client id
	 * @return all Messages the Client has not already received, or all if no messages were requested in t sec.
	 * @throws RemoteException
	 */
	public String nextMessage(String clientID)                 throws RemoteException;
	
	
	/**
	 * New message.
	 *
	 * @param clientID the client id
	 * @param message the message
	 * @throws RemoteException
	 */
	public void   newMessage (String clientID, String message) throws RemoteException;
}
