/**
 * 
 */
package common;

import java.sql.Date;

/**
 * Project: VS_1
 *
 * @author Andreas M�ller
 * @MatrNr 209 918 2
 *
 * 24.09.2016
 */
public class Message {
	private String clientID;
	public int ID;
	private String message;
	private int timestamp;
	
	public Message(String clientID, int id, String message) {
		this.clientID = clientID;
		ID = id;
		this.message = message;
		timestamp = (int)(System.currentTimeMillis() / 1000L);
	}
	
	public String getRawMessage () {
		return message;
	}
	
	public String getFormatedMessage(){
		Date date = new Date((long)(timestamp * 1000L));
		String format = ID        + " " + 
						clientID  + ":" +
						message   + " " +
						date      + "";
		
		return format;
	}
	
}
