package client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import common.*;

public class ComModule {
	private MessageService stub;

	public ComModule(String host) {
		try {
			Registry registry = LocateRegistry.getRegistry(host);
			stub = (MessageService) registry.lookup("server");
		} catch (Exception e) {
			boolean ok = false;
			for (int i = 0; i < 10; i++) {
				try {
					Registry registry = LocateRegistry.getRegistry(host);
					stub = (MessageService) registry.lookup("server");
					ok = true;
					break;
				} catch (RemoteException | NotBoundException e2) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e3) {

					}
				}
			}
			if (!ok) {
				System.out.println("Connection timeout, Server doesn't respond");
			}
		}
	}

	public void newMessage(String clientID, String message) throws RemoteException {
		if (stub != null) {
			stub.newMessage(clientID, message);
		}
	}

	public String response(String clientID) throws RemoteException {
		if (stub != null) {
			return stub.nextMessage(clientID);
		}
		return null;
	}

}
