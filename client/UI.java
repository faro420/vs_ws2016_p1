package client;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class UI extends JPanel {

	/**
	 * 
	 */
	private ComModule com;
	private static final long serialVersionUID = 1L;
	protected JTextField textField1, textField3;
	protected JTextArea textArea;
	protected JLabel label1, label2, label3;
	protected JButton button1, button2;;
	protected String clientID;
	private final static String newline = "\n";

	public UI() {
		super(new GridBagLayout());
		try {
			clientID = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e2) {
			e2.printStackTrace();
		}
		label1 = new JLabel("Server IP");
		label2 = new JLabel("Client IP: " + clientID);
		label3 = new JLabel("Message to be Sent");
		JButton button1 = new JButton("Send a Message");
		JButton button2 = new JButton("Receive Message/ Messages");

		textArea = new JTextArea(5, 20);
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.append("");
		textArea.setCaretPosition(textArea.getDocument().getLength());

		textField1 = new JTextField(50);
		textField3 = new JTextField(50);

		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String host = textField1.getText();
				com = new ComModule(host);
				if (com != null) {
					try {
						com.newMessage(clientID, textField3.getText());
					} catch (RemoteException e1) {

					}
				}
			}
		});

		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String host = textField1.getText();
				com = new ComModule(host);

				String message = "";
				if (com != null) {
					while (message != null) {
						try {
							message = com.response(clientID);
						} catch (RemoteException e1) {

						}
						if (message != null)
							textArea.append(message + newline);
					}
				}
			}
		});

		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = GridBagConstraints.REMAINDER;

		c.fill = GridBagConstraints.HORIZONTAL;
		add(label1, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		add(textField1, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		add(label2, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		add(button1, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		add(label3, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		add(textField3, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		add(button2, c);

		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.weighty = 1.0;
		add(scrollPane, c);
	}

	private static void createGUI() {
		JFrame frame = new JFrame("Message Service");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(new UI());
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createGUI();
			}
		});
	}
}
