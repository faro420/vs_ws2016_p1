package server;

/**
 * Project: VS_1
 *
 * @author Andreas Mueller
 * @MatrNr 209 918 2
 *
 * 26.09.2016
 */

public class ClientStateHandle {
	public final String clientID;
	public int timestamp; // timestamp the last message was requested /UNIX-Timestamp)
	public int lastMsgID; // ID of last message requested
	
	public ClientStateHandle(String clientID, int msgID){
		this.clientID = clientID;
		timestamp = (int)(System.currentTimeMillis() / 1000L);
		lastMsgID = msgID;
	}
	
	public boolean isStillValid(int currentTime) {
		// Check if handle is valid
		if(timestamp + Server.handleTimeInSec >= currentTime) {
			timestamp = currentTime;
			return true;
		} else {
			timestamp = currentTime;
			return false;
		}
	}
}
