/**
 * 
 */
package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import common.Message;
import common.MessageService;

/**
 * Project: VS_1
 *
 * @author Andreas Mueller
 * @MatrNr 209 918 2
 *
 * 24.09.2016
 */
public class Server extends UnicastRemoteObject implements MessageService {

	public static final int handleTimeInSec = 10; // time the handle is valid

	
	private static final boolean SHOWDEBUG = true;
	private static final int FIFOSIZE = 10; // size of the Buffer
	
	/* !!!!! IMPORTANT !!!!!
	*	The Path to the server policy has to be adjusted !!! */
	private static final String policyFile = "file:/mnt/fileserver/MyHome/TI_Labor/Linux/eclipse_mars/vs_p1/src/server.policy";
	

	private Message[] messages_ = new Message[FIFOSIZE]; // Message Buffer
	private int currentIndex = 0; // Write index of FIFO
	private int messageID = 1; // current message ID
	
	private ArrayList<ClientStateHandle> handles = new ArrayList<ClientStateHandle>(); // Handle List
	
	
	public Server() throws RemoteException {
		super();
	}
	
	
	/* (non-Javadoc)
	 * @see server.MessageService#nextMessage(java.lang.String)
	 */
	@Override
	public String nextMessage(String clientID) throws RemoteException {
		ClientStateHandle handle = null;
		
		// 'returnString' will stay null if no new not already send message is on server for the Client
		String returnString = null;
		
		// Check if handle exists for Client
		for (ClientStateHandle clientStateHandle : handles) {
			if (clientStateHandle.clientID.equals(clientID)){
				if(SHOWDEBUG) System.out.println("Handle found!");
				handle = clientStateHandle;
				break;
			}
		}
		
		// Create a new Handle for new Clients
		if(handle == null) {
			handle = new ClientStateHandle(clientID, 0);
			handles.add(handle);
		}
		
		// Check if handle is still valid
		if(handle.isStillValid((int)(System.currentTimeMillis() / 1000L))) {
			// Handle is valid, now send the oldest not already sended message
			if(SHOWDEBUG) System.out.println("Handle valid!");
			final int lastId = handle.lastMsgID;
			
			// Iterate through FIFO-Buffer until a valid message is found (message id > last id -> newer)
			for(int i = 0; i < FIFOSIZE; i++) {
				// message Index is current index (write index) + 1 (MOD Size) to start at the oldest
				final int idx = (currentIndex + i) % FIFOSIZE;
				
				if(messages_[idx] != null && lastId < messages_[idx].ID) {
					// Valid message found
					returnString = messages_[idx].getFormatedMessage();
					if(SHOWDEBUG) System.out.println("Handle change ["+lastId+" -> "+messages_[idx].ID+"]");
					handle.lastMsgID = messages_[idx].ID;
					break; // for loop
				}
			} // for loop
			
		} else {
			
			// Handle is NOT valid, now send the oldest message
			if(SHOWDEBUG) System.out.println("Handle NOT valid!");
			
			// Iterate through FIFO-Buffer until a valid message is found (message not null)
			for(int i = 0; i < FIFOSIZE; i++) {
				// message Index is current index (write index) + 1 (MOD Size) to start at the oldest
				final int idx = (currentIndex + i) % FIFOSIZE;
				
				if(messages_[idx] != null) {
					// Valid message found
					returnString = messages_[idx].getFormatedMessage();
					handle.lastMsgID = messages_[idx].ID;
					break; // for loop
				}
			} // for loop
		} // if-else
		
		return returnString;
	}

	
	/* (non-Javadoc)
	 * @see server.MessageService#newMessage(java.lang.String, java.lang.String)
	 */
	@Override
	public void newMessage(String clientID, String message)
			throws RemoteException {
		// Write new Message into the FIFO-Buffer
		messages_[currentIndex] = new Message(clientID, messageID, message);
		
		// Calculate next write Index of FIFO-Buffer
		currentIndex++;
		currentIndex = currentIndex % FIFOSIZE;
		
		// Increment Message ID
		messageID++;
	}

	
	
	public static void main (String [] args){
		System.setProperty("java.security.policy", policyFile);
		
		if( System.getSecurityManager() == null ) System.setSecurityManager(new SecurityManager());
		
		try {	
			// Create server implementation
			MessageService server = new Server();

			// Create Registry for standart Port (1099)
			Registry registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
			
			// Rebind server implementation Object to name "server"
			registry.rebind("server", server);
			
			System.out.println("Server initialized!");
			
		} catch(RemoteException e){
			e.printStackTrace();
		}
		
		while(true);
	}
}
